package com.x1.tests.social;

import com.x1.tests.BaseTestCase;
import com.x1.tests.steps.HomeSteps;
import com.x1.tests.steps.PreconditionSteps;
import com.x1.tests.steps.social.TwitterSteps;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @Author Anna Dvarri
 * Twitter Button Tests<br>
 * This test verifies that if click 'Twitter' button move to 'Twitter' page of 'Kreditech' company
 */
public class TwitterButtonTest extends BaseTestCase {

    private HomeSteps homeSteps;

    /**
     * Precondition method make search in Google and go to Home Page
     * */

    @BeforeClass
    public void searchKreditechAndGoToHomePage(){
        PreconditionSteps preconditionSteps = new PreconditionSteps(getDriver());
        homeSteps = preconditionSteps.searchKreditechAndGoToHomePage();
    }

    /*
    *  This test verifies that if click 'Twitter' button
    *  move to 'Twitter' page of 'Kreditech' company
    * */

    @Test
    public void verifyThatTwitterPageIsDisplayedAndRelatedToKreditech(){
        TwitterSteps twitterSteps = homeSteps.openTwitterPageInNewWindow();
        twitterSteps.verifyTwitterUrlIsCorrect();
        twitterSteps.verifyKreditechTwitterPageIsDisplayed();
    }
}
