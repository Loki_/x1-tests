package com.x1.tests.social;

import com.x1.tests.BaseTestCase;
import com.x1.tests.steps.HomeSteps;
import com.x1.tests.steps.PreconditionSteps;
import com.x1.tests.steps.social.FacebookSteps;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @Author Anna Dvarri
 * Facebook Button Tests<br>
 * This test verifies that if click 'Facebook' button move to 'Facebook' page of 'Kreditech' company
 */
public class FacebookButtonTest extends BaseTestCase {

    private HomeSteps homeSteps;

    /**
     * Precondition method make search in Google and go to Home Page
     * */

    @BeforeClass
    public void searchKreditechAndGoToHomePage(){
        PreconditionSteps preconditionSteps = new PreconditionSteps(getDriver());
        homeSteps = preconditionSteps.searchKreditechAndGoToHomePage();
    }

    /*
    *  This test verifies that if click 'Facebook' button
    *  move to 'Facebook' page of 'Kreditech' company
    * */

    @Test
    public void verifyThatFacebookPageIsDisplayedAndRelatedToKreditech(){
        FacebookSteps facebookSteps = homeSteps.openFacebookPageInNewWindow();
        facebookSteps.loginIntoFacebook();
        facebookSteps.verifyFacebookUrlIsCorrect();
        facebookSteps.verifyKreditechFacebookPageIsDisplayed();
    }
}
