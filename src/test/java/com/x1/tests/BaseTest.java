package com.x1.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Created by anna on 15.08.17.
 */
public class BaseTest {

    protected WebDriver getDriver() {
        return driver;
    }

    private static final String sFileName = "testBrowser.properties";
    private static String sDirSeparator = System.getProperty("file.separator");
    private static Properties props = new Properties();
    public WebDriver driver;

    public WebDriver chooseBrowsers() throws IOException {
        File currentDir = new File(".");

        String sFilePath = currentDir.getCanonicalPath() + sDirSeparator + sFileName;
        FileInputStream ins = new FileInputStream(sFilePath);
        props.load(ins);
        String browser = props.getProperty("BROWSER");

        switch (browser) {
            case "Chrome":
                return createChromeDriver();
            default:
                return createFirefoxDriver();
        }

    }
    private WebDriver createChromeDriver() {
        driver = new ChromeDriver();
        return driver;
    }
    private WebDriver createFirefoxDriver() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver;
    }

}
