package com.x1.tests.navigation;

import com.x1.tests.BaseTestCase;
import com.x1.tests.steps.HomeSteps;
import com.x1.tests.steps.PreconditionSteps;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @Author Anna Dvarri
 * Navigation Bar Tests<br>
 * These tests verify that 'Navigation Bar' functional works correctly
 */
public class NavigationTest extends BaseTestCase{

    private HomeSteps homeSteps;

    /**
     * Precondition method make move to Home Page
     * */

    @BeforeClass
    public void searchKreditechAndGoToHomePage(){
        PreconditionSteps preconditionSteps = new PreconditionSteps(getDriver());
        homeSteps = preconditionSteps.openKreditech();
    }

    /*
    *  This test verifies that if click 'What We Do' button
    *  move to 'What We Do' page of 'Kreditech' site
    * */

    @Test
    public void verifyThatWhatWeDoPageIsDisplayed(){
        homeSteps.verifyWhatWeDoButtonIsDisplayed();
        homeSteps.verifyWhatWeDoPageIsDisplayed();
    }

    /*
    *  This test verifies that if click 'Who We Are' button
    *  move to 'Who We Are' page of 'Kreditech' site
    * */

    @Test
    public void verifyThatWhoWeArePageIsDisplayed(){
        homeSteps.verifyWhoWeAreButtonIsDisplayed();
        homeSteps.verifyWhoWeArePageIsDisplayed();
    }

    /*
    *  This test verifies that if click 'Work With Us' button
    *  move to 'Work With Us' page of 'Kreditech' site
    * */

    @Test
    public void verifyThatWorkWithUsPageIsDisplayed(){
        homeSteps.verifyWorkWithUsButtonIsDisplayed();
        homeSteps.verifyWorkWithUsPageIsDisplayed();
    }

    /*
    *  This test verifies that if click 'Careers' button
    *  move to 'Careers' page of 'Kreditech' site
    * */

    @Test
    public void verifyThatCareersPageIsDisplayed(){
        homeSteps.verifyCareersButtonIsDisplayed();
        homeSteps.verifyCareersPageIsDisplayed();
    }

    /*
    *  This test verifies that if click 'Investor Relation' button
    *  move to 'Investor Relation' page of 'Kreditech' site
    * */

    @Test
    public void verifyThatInvestorRelationPageIsDisplayed(){
        homeSteps.verifyInvestorRelationButtonIsDisplayed();
        homeSteps.verifyInvestorRelationPageIsDisplayed();
    }

    /*
    *  This test verifies that if click 'Investor Relation' button
    *  move to 'Investor Relation' page of 'Kreditech' site
    * */

    @Test
    public void verifyThatPressPageIsDisplayed(){
        homeSteps.verifyPressButtonIsDisplayed();
        homeSteps.verifyPressPageIsDisplayed();
    }

    /*
    *  This test verifies that if click 'Investor Relation' button
    *  move to 'Investor Relation' page of 'Kreditech' site
    * */

    @Test
    public void verifyThatMagazinePageIsDisplayed(){
        homeSteps.verifyMagazineButtonIsDisplayed();
        homeSteps.verifyMagazinePageIsDisplayed();
    }
}
