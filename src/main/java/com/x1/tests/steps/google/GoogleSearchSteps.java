package com.x1.tests.steps.google;

import com.x1.tests.pages.google.GoogleResultsPage;
import com.x1.tests.pages.google.GoogleSearchPage;
import lombok.extern.slf4j.Slf4j;

import static org.testng.Assert.assertTrue;

/**
 * @author Anna Dvarri
 * Google Search Steps<br>
 * This class contains steps methods, which are actions with web elements on 'Google Search' page<br>
 */
@Slf4j
public class GoogleSearchSteps{

    private GoogleSearchPage googleSearchPage;
    private GoogleResultsPage googleResultsPage;

    public GoogleSearchSteps(GoogleSearchPage googleSearchPage){
        this.googleSearchPage = googleSearchPage;
    }

    public void verifyGoogleSearchPageIsDisplayed() {
        assertTrue(googleSearchPage.isDisplayedSearchPage(), "'Google Search' page is not displayed");
    }

    public void openGoogleSearch(){
        googleSearchPage.openGoogleSearchPage();
    }

    public void typeTextInSearchField(String text){
        googleSearchPage.typeInSearchField(text);
    }

    public GoogleResultsSteps clickSearchButton(){
        googleResultsPage = googleSearchPage.clickSearchButon();
        return new GoogleResultsSteps(googleResultsPage);
    }

}
