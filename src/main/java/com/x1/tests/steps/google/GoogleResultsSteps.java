package com.x1.tests.steps.google;

import com.x1.tests.pages.google.GoogleResultsPage;
import com.x1.tests.pages.HomePage;
import com.x1.tests.steps.HomeSteps;
import lombok.extern.slf4j.Slf4j;

import static org.testng.Assert.assertTrue;

/**
 * @author Anna Dvarri
 * Google Results Steps<br>
 * This class contains steps methods, which are actions with web elements on 'Google Results' page<br>
 */
@Slf4j
public class GoogleResultsSteps {

    private GoogleResultsPage googleResultsPage;
    private HomePage homePage;

    public GoogleResultsSteps(GoogleResultsPage googleResultsPage){
        this.googleResultsPage = googleResultsPage;
    }

    public void verifyGoogleResultsPageIsDisplayed() {
        assertTrue(googleResultsPage.isDisplayedResultsPage(), "'Google Results' page is not displayed");
    }

    public HomeSteps clickKreditechLink(){
        homePage = googleResultsPage.clickKreditechLink();
        return new HomeSteps(homePage);
    }
}
