package com.x1.tests.steps;

import com.x1.tests.pages.HomePage;
import com.x1.tests.pages.social.FacebookPage;
import com.x1.tests.pages.social.LinkedInPage;
import com.x1.tests.pages.social.TwitterPage;
import com.x1.tests.steps.social.FacebookSteps;
import com.x1.tests.steps.social.LinkedInSteps;
import com.x1.tests.steps.social.TwitterSteps;
import lombok.extern.slf4j.Slf4j;;

import static org.testng.Assert.assertTrue;

/**
 * @author Anna Dvarri
 * Home Steps<br>
 * This class contains steps methods, which are actions with web elements on 'Home' page<br>
 */
@Slf4j
public class HomeSteps{

    private HomePage homePage;
    private LinkedInPage linkedInPage;
    private FacebookPage facebookPage;
    private TwitterPage twitterPage;

    public HomeSteps(HomePage homePage){
        this.homePage = homePage;
    }

    public void verifyHomePageIsDisplayed() {
        assertTrue(homePage.isDisplayedHomePage(), "'Home' page is not displayed");
    }

    public HomeSteps openHomePage(){
        homePage.openHomePage();
        return new HomeSteps(homePage);
    }

    public void verifyHomeUrlIsCorrect() {
        assertTrue(homePage.isCorrectHomeUrl(), "'Home' url is not correct");
    }

    public TwitterSteps openTwitterPageInNewWindow(){
        TwitterPage twitterPage = homePage.openTwitterPageInNewWindow();
        homePage.waitElement();
        return new TwitterSteps(twitterPage);
    }

    public FacebookSteps openFacebookPageInNewWindow(){
        FacebookPage facebookPage = homePage.openFacebookPageInNewWindow();
        homePage.waitElement();
        return new FacebookSteps(facebookPage);
    }

    public LinkedInSteps openLinkedInPageInNewWindow(){
        LinkedInPage linkedInPage = homePage.openLinkedInPageInNewWindow();
        homePage.waitElement();
        return new LinkedInSteps(linkedInPage);
    }

    public void verifyWhatWeDoButtonIsDisplayed(){
        assertTrue(homePage.isDisplayedWhatWeDoButton(), "'What We Do' button is not displayed");
    }

    public void verifyWhoWeAreButtonIsDisplayed(){
        assertTrue(homePage.isDisplayedWhoWeAreButton(), "'Who We Are' button is not displayed");
    }

    public void verifyWorkWithUsButtonIsDisplayed(){
        assertTrue(homePage.isDisplayedWorkWithUsButton(), "'Work With Us' button is not displayed");
    }

    public void verifyCareersButtonIsDisplayed(){
        assertTrue(homePage.isDisplayedCareersButton(), "'Careers' button is not displayed");
    }

    public void verifyInvestorRelationButtonIsDisplayed(){
        assertTrue(homePage.isDisplayedInvestorRelationButton(), "'Investor Relation' button is not displayed");
    }

    public void verifyPressButtonIsDisplayed(){
        assertTrue(homePage.isDisplayedPressButton(), "'Press' button is not displayed");
    }

    public void verifyMagazineButtonIsDisplayed(){
        assertTrue(homePage.isDisplayedMagazineButton(), "'Magazine' button is not displayed");
    }

    public void verifyWhatWeDoPageIsDisplayed(){
        homePage.clickWhatWeDoButton();
        assertTrue(homePage.isDisplayedWhatWeDoPage(), "'What We Do' page is not displayed");
    }

    public void verifyWhoWeArePageIsDisplayed(){
        homePage.clickWhoWeAreButton();
        assertTrue(homePage.isDisplayedWhoWeArePage(), "'Who We Are' page is not displayed");
    }

    public void verifyWorkWithUsPageIsDisplayed(){
        homePage.clickWorkWithUsButton();
        assertTrue(homePage.isDisplayedWorkWithUsPage(), "'Work With Us' page is not displayed");
    }

    public void verifyCareersPageIsDisplayed(){
        homePage.clickCareersButton();
        assertTrue(homePage.isDisplayedCareersPage(), "'Careers' page is not displayed");
    }

    public void verifyInvestorRelationPageIsDisplayed(){
        homePage.clickInvestorRelationButton();
        assertTrue(homePage.isDisplayedInvestorRelationPage(), "'Investor Relation' page is not displayed");
    }

    public void verifyPressPageIsDisplayed(){
        homePage.clickPressButton();
        assertTrue(homePage.isDisplayedPressPage(), "'Press' page is not displayed");
    }

    public void verifyMagazinePageIsDisplayed(){
        homePage.clickMagazineButton();
        assertTrue(homePage.isDisplayedMagazinePage(), "'Magazine' page is not displayed");
    }
}
