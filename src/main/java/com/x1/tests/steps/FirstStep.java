package com.x1.tests.steps;

import org.openqa.selenium.WebDriver;

/**
 * Created by anna on 18.08.17.
 */
public class FirstStep {

    private WebDriver driver;

    public FirstStep(WebDriver driver){
        this.driver = driver;
    }

    protected WebDriver getDriver() {
        return driver;
    }
}
