package com.x1.tests.steps;

import com.x1.tests.pages.HomePage;
import com.x1.tests.pages.google.GoogleSearchPage;
import com.x1.tests.steps.google.GoogleResultsSteps;
import com.x1.tests.steps.google.GoogleSearchSteps;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by anna on 18.08.17.
 */
public class PreconditionSteps extends FirstStep{

    private HomePage homePage = new HomePage(getDriver());
    private HomeSteps homeSteps = new HomeSteps(homePage);
    private GoogleSearchSteps googleSearchSteps;
    private GoogleResultsSteps googleResultsSteps;
    private String searchText = "kreditech";

    public PreconditionSteps(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public HomeSteps searchKreditechAndGoToHomePage() {
        GoogleSearchPage googleSearchPage = new GoogleSearchPage(getDriver());
        googleSearchSteps = new GoogleSearchSteps(googleSearchPage);
        googleSearchSteps.openGoogleSearch();
        googleSearchSteps.verifyGoogleSearchPageIsDisplayed();
        googleSearchSteps.typeTextInSearchField(searchText);
        googleResultsSteps = googleSearchSteps.clickSearchButton();
        homeSteps = googleResultsSteps.clickKreditechLink();
        homeSteps.verifyHomePageIsDisplayed();
        return homeSteps;
    }

    public HomeSteps openKreditech(){
        homeSteps.openHomePage();
        return homeSteps;
    }
}
