package com.x1.tests.steps.social;

import com.x1.tests.pages.social.FacebookPage;
import lombok.extern.slf4j.Slf4j;

import static org.testng.Assert.assertTrue;

/**
 * @author Anna Dvarri
 * Facebook Steps<br>
 * This class contains steps methods, which are actions with web elements on 'Facebook' page<br>
 */
@Slf4j
public class FacebookSteps {

    private FacebookPage facebookPage;

    public FacebookSteps(FacebookPage facebookPage){
        this.facebookPage = facebookPage;
    }

    public void verifyFacebookPageIsDisplayed() {
        assertTrue(facebookPage.isDisplayedFacebookPage(), "'Facebook' page is not displayed");
    }

    public void verifyKreditechFacebookPageIsDisplayed(){
        assertTrue(facebookPage.isDisplayedKreditechFacebookPage(), "'Kreditech Facebook' page is not displayed");
    }

    public void verifyFacebookUrlIsCorrect() {
        assertTrue(facebookPage.isCorrectFacebookUrl(), "'Facebook' url is not correct");
    }

    public void loginIntoFacebook(){
        facebookPage.typeEmail();
        facebookPage.typePassword();
        facebookPage.clickLoginButton();
    }
}
