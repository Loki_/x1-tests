package com.x1.tests.steps.social;

import com.x1.tests.pages.social.LinkedInPage;
import lombok.extern.slf4j.Slf4j;

import static org.testng.Assert.assertTrue;

/**
 * @author Anna Dvarri
 * LinkedIn Steps<br>
 * This class contains steps methods, which are actions with web elements on 'LinkedIn' page<br>
 */
@Slf4j
public class LinkedInSteps {

    private LinkedInPage linkedInPage;

    public LinkedInSteps(LinkedInPage linkedInPage){
        this.linkedInPage = linkedInPage;
    }

    public void verifyLinkedInPageIsDisplayed() {
                assertTrue(linkedInPage.isDisplayedLinkedInPage(), "'LinkedIn' page is not displayed");
    }

    public void verifyKreditechLinkedInPageIsDisplayed(){
        assertTrue(linkedInPage.isDisplayedKreditechLinkedInPage(), "'Kreditech LinkedIn' page is not displayed");
    }

    public void verifyLinkedInUrlIsCorrect() {
        assertTrue(linkedInPage.isCorrectLinkedInUrl(), "'LinkedIn' url is not correct");
    }

}

