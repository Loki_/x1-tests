package com.x1.tests.steps.social;

import com.x1.tests.pages.social.TwitterPage;
import lombok.extern.slf4j.Slf4j;

import static org.testng.Assert.assertTrue;

/**
 * @author Anna Dvarri
 * Twitter Steps<br>
 * This class contains steps methods, which are actions with web elements on 'Twitter' page<br>
 */
@Slf4j
public class TwitterSteps {

    private TwitterPage twitterPage;

    public TwitterSteps(TwitterPage twitterPage){
        this.twitterPage = twitterPage;
    }

    public void verifyTwitterPageIsDisplayed() {
        assertTrue(twitterPage.isDisplayedTwitterPage(), "'Twitter' page is not displayed");
    }

    public void verifyKreditechTwitterPageIsDisplayed(){
        assertTrue(twitterPage.isDisplayedKreditechTwitterPage(), "'Kreditech Twitter' page is not displayed");
    }

    public void verifyTwitterUrlIsCorrect() {
        assertTrue(twitterPage.isCorrectTwitterUrl(), "'Twitter' url is not correct");
    }
}
