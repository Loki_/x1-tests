package com.x1.tests.pages.google;

import com.x1.tests.pages.HomePage;
import com.x1.tests.pages.WebBasePage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author Anna Dvarri
 * Implementing methods for interacting with elements of Google results page
 */
@Slf4j
public class GoogleResultsPage extends WebBasePage {

    private static final String GOOGLE_RESULTS_PAGE_XPATH = "//div[@id='resultStats']";
    private static final String KREDITECH_LINK_XPATH = "//a[@href='https://www.kreditech.com/']";

    public GoogleResultsPage (WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy (xpath = GOOGLE_RESULTS_PAGE_XPATH)
    private WebElement googleResultsPage;

    @FindBy (xpath = KREDITECH_LINK_XPATH)
    private WebElement kreditechLink;

    public boolean isDisplayedResultsPage(){
        return googleResultsPage.isDisplayed();
    }

    public HomePage clickKreditechLink(){
        kreditechLink.click();
        return new HomePage(getDriver());
    }
}
