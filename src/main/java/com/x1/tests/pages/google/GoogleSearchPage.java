package com.x1.tests.pages.google;

import com.x1.tests.pages.WebBasePage;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author Anna Dvarri
 * Implementing methods for interacting with elements of Google search page
 */
@Slf4j
public class GoogleSearchPage extends WebBasePage {

    private static final String URL = "https://www.google.com";

    private static final String GOOGLE_SEARCH_PAGE_XPATH = "//div[@class='ctr-p']";
    private static final String SEARCH_FIELD_XPATH = "//input[@id='lst-ib']";
    private static final String SEARCH_BUTTON_XPATH = "//input[@class='lsb']";

    public GoogleSearchPage (WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy (xpath = GOOGLE_SEARCH_PAGE_XPATH)
    private WebElement googleSearchPage;

    @FindBy (xpath = SEARCH_FIELD_XPATH)
    @Getter
    private WebElement searchField;

    @FindBy (xpath = SEARCH_BUTTON_XPATH)
    private WebElement searchButton;

    public boolean isDisplayedSearchPage(){
        return googleSearchPage.isDisplayed();
    }

    public GoogleSearchPage openGoogleSearchPage() {
        getDriver().get(URL);
        return this;
    }

    public void typeInSearchField(String text) {
        searchField.click();
        searchField.clear();
        searchField.sendKeys(text);
    }

    public GoogleResultsPage clickSearchButon(){
        searchButton.click();
        GoogleResultsPage googleResultsPage = new GoogleResultsPage(getDriver());
        return googleResultsPage;
    }


}
