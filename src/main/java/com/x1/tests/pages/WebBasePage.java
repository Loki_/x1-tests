package com.x1.tests.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by anna on 15.08.17.
 */
public abstract class WebBasePage {

    private WebDriver driver;;

    public WebBasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    protected WebDriver getDriver() {
        return driver;
    }

    public void waitElement(){
        getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    public void switchToNewTab(){
        for (String windowHandler : driver.getWindowHandles()) {
            driver.switchTo().window(windowHandler);        }
    }

}
