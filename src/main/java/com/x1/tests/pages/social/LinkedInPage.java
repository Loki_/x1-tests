package com.x1.tests.pages.social;

import com.x1.tests.pages.WebBasePage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author Anna Dvarri
 * Implementing methods for interacting with elements of LinkedIn page
 */
@Slf4j
public class LinkedInPage extends WebBasePage {

    private static final String LINKEDIN_PAGE_XPATH = "//nav[@id='extended-nav']";
    private static final String KREDITECH_LINKEDIN_PAGE_XPATH = "//h1[contains(text(), 'Kreditech Holding SSL GmbH')]";

    public LinkedInPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy (xpath = LINKEDIN_PAGE_XPATH)
    private WebElement linkedInPage;

    @FindBy (xpath = KREDITECH_LINKEDIN_PAGE_XPATH)
    private WebElement kreditechLinkedInPage;

    public boolean isDisplayedLinkedInPage(){
        return linkedInPage.isDisplayed();
    }

    public boolean isDisplayedKreditechLinkedInPage(){
        return kreditechLinkedInPage.isDisplayed();
    }

    public boolean isCorrectLinkedInUrl(){
        return getDriver().getCurrentUrl().equals("https://www.linkedin.com/company-beta/2660395");
    }
}
