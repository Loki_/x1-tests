package com.x1.tests.pages.social;

import com.x1.tests.pages.WebBasePage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author Anna Dvarri
 * Implementing methods for interacting with elements of Facebook page
 */
@Slf4j
public class FacebookPage extends WebBasePage {

    private final static String FACEBOOK_PAGE_XPATH = "//span[text()='Facebook']";
    private final static String KREDITECH_FACEBOOK_PAGE_XPATH = "//a[text()='@kreditech']";

    private final static String EMAIL_INPUT_XPATH = "//input[@id='email']";
    private final static String PASSWORD_INPUT_XPATH = "//input[@id='pass']";
    private final static String LOGIN_BUTTON_XPATH = "//button[@id='loginbutton']";

    public FacebookPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy (xpath = FACEBOOK_PAGE_XPATH)
    private WebElement facebookPage;

    @FindBy (xpath = KREDITECH_FACEBOOK_PAGE_XPATH)
    private WebElement kreditechFacebookPage;

    @FindBy (xpath = EMAIL_INPUT_XPATH)
    private WebElement emailInput;

    @FindBy (xpath = PASSWORD_INPUT_XPATH)
    private WebElement passwordInput;

    @FindBy (xpath = LOGIN_BUTTON_XPATH)
    private WebElement loginButton;

    public boolean isDisplayedFacebookPage(){
        return facebookPage.isDisplayed();
    }

    public boolean isDisplayedKreditechFacebookPage(){
        return kreditechFacebookPage.isDisplayed();
    }

    public boolean isCorrectFacebookUrl(){
        return getDriver().getCurrentUrl().equals("https://www.facebook.com/kreditech/");
    }

    public void typeEmail(){
        emailInput.clear();
        emailInput.sendKeys("");
    }

    public void typePassword(){
        passwordInput.clear();
        passwordInput.sendKeys("");
    }

    public void clickLoginButton(){
        loginButton.click();
    }
}
