package com.x1.tests.pages.social;

import com.x1.tests.pages.WebBasePage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author Anna Dvarri
 * Implementing methods for interacting with elements of Twitter page
 */
@Slf4j
public class TwitterPage extends WebBasePage {

    private final static String TWITTER_PAGE_XPATH = "//div[contains(@class, 'global-nav--newLoggedOut')]";
    private final static String KREDITECH_TWITTER_PAGE_XPATH = "//h1/a[text()='Kreditech']";

    public TwitterPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy (xpath = TWITTER_PAGE_XPATH)
    private WebElement twitterPage;

    @FindBy (xpath = KREDITECH_TWITTER_PAGE_XPATH)
    private WebElement kreditechTwitterPage;

    public boolean isDisplayedTwitterPage(){
        return twitterPage.isDisplayed();
    }

    public boolean isDisplayedKreditechTwitterPage(){
        return kreditechTwitterPage.isDisplayed();
    }

    public boolean isCorrectTwitterUrl(){
        return getDriver().getCurrentUrl().equals("https://twitter.com/kreditech");
    }
}
