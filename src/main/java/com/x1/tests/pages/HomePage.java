package com.x1.tests.pages;

import com.x1.tests.pages.social.FacebookPage;
import com.x1.tests.pages.social.LinkedInPage;
import com.x1.tests.pages.social.TwitterPage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author Anna Dvarri
 * Implementing methods for interacting with elements of Home page
 */
@Slf4j
public class HomePage extends WebBasePage{

    private static final String HOME_URL = "https://www.kreditech.com/";

    private static final String HOME_PAGE_XPATH = "//div[@id='main']";

    private static final String LINKEDIN_BUTTON_XPATH = "//div[@class = 'social linkedin']";
    private static final String FACEBOOK_BUTTON_XPATH = "//div[@class = 'social facebook']";
    private static final String TWITTER_BUTTON_XPATH = "//div[@class = 'social twitter']";

    private static final String WHAT_WE_DO_BUTTON_XPATH = "//a[contains(text(), 'What we do')]";
    private static final String WHO_WE_ARE_BUTTON_XPATH = "//a[contains(text(), 'Who we are')]";
    private static final String WORK_WITH_US_BUTTON_XPATH = "//a[contains(text(), 'Work with us')]";
    private static final String CAREERS_BUTTON_XPATH = "//a[contains(text(), 'Careers')]";
    private static final String INVESTOR_RELATIONS_BUTTON_XPATH = "//a[contains(text(), 'Investor Relations')]";
    private static final String PRESS_BUTTON_XPATH = "//a[contains(text(), 'Press')]";
    private static final String MAGAZINE_BUTTON_XPATH = "//a[contains(text(), 'Magazine')]";

    private static final String WHAT_WE_DO_PAGE_XPATH = "//div[@class='what-we-do']";
    private static final String WHO_WE_ARE_PAGE_XPATH = "//div[@class='who-we-are']";
    private static final String WORK_WITH_US_PAGE_XPATH = "//div[@class='work-with-us']";
    private static final String CAREERS_PAGE_XPATH = "//div[contains(@class, 'careers')]";
    private static final String INVESTOR_RELATIONS_PAGE_XPATH = "//div[@class='investor-relations']";
    private static final String PRESS_PAGE_XPATH = "//div[@class='press']";
    private static final String MAGAZINE_PAGE_XPATH = "//div[@class='magazine']";

    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy (xpath = HOME_PAGE_XPATH)
    private WebElement homePage;

    @FindBy (xpath = LINKEDIN_BUTTON_XPATH)
    private WebElement linkedInButton;

    @FindBy (xpath = FACEBOOK_BUTTON_XPATH)
    private WebElement facebookButton;

    @FindBy (xpath = TWITTER_BUTTON_XPATH)
    private WebElement twitterButton;

    @FindBy (xpath = WHAT_WE_DO_BUTTON_XPATH)
    private WebElement whatWeDoButton;

    @FindBy (xpath = WHO_WE_ARE_BUTTON_XPATH)
    private WebElement whoWeAreButton;

    @FindBy (xpath = WORK_WITH_US_BUTTON_XPATH)
    private WebElement workWithUsButton;

    @FindBy (xpath = CAREERS_BUTTON_XPATH)
    private WebElement careersButton;

    @FindBy (xpath = INVESTOR_RELATIONS_BUTTON_XPATH)
    private WebElement investorRelationsButton;

    @FindBy (xpath = PRESS_BUTTON_XPATH)
    private WebElement pressButton;

    @FindBy (xpath = MAGAZINE_BUTTON_XPATH)
    private WebElement magazineButton;

    @FindBy (xpath = WHAT_WE_DO_PAGE_XPATH)
    private WebElement whatWeDoPage;

    @FindBy (xpath = WHO_WE_ARE_PAGE_XPATH)
    private WebElement whoWeArePage;

    @FindBy (xpath = WORK_WITH_US_PAGE_XPATH)
    private WebElement workWithUsPage;

    @FindBy (xpath = CAREERS_PAGE_XPATH)
    private WebElement careersPage;

    @FindBy (xpath = INVESTOR_RELATIONS_PAGE_XPATH)
    private WebElement investorRelationsPage;

    @FindBy (xpath = PRESS_PAGE_XPATH)
    private WebElement pressPage;

    @FindBy (xpath = MAGAZINE_PAGE_XPATH)
    private WebElement magazinePage;

    public HomePage openHomePage() {
        getDriver().get(HOME_URL);
        waitElement();
        return this;
    }

    public boolean isDisplayedHomePage(){
        return homePage.isDisplayed();
    }

    public boolean isCorrectHomeUrl(){
        return getDriver().getCurrentUrl().equals("https://www.kreditech.com");
    }

    public boolean isDisplayedLinkedInButton(){
        return linkedInButton.isDisplayed();
    }

    public boolean isDisplayedFacebookButton(){
        return facebookButton.isDisplayed();
    }

    public boolean isDisplayedTwitterButton(){
        return twitterButton.isDisplayed();
    }

    public void clickLinkedInButton(){
        linkedInButton.click();
    }

    public void clickFacebookButton(){
        facebookButton.click();
    }

    public void clickTwitterButton(){
        twitterButton.click();
    }

    public LinkedInPage openLinkedInPageInNewWindow() {
        linkedInButton.click();
        waitElement();
        switchToNewTab();
        return new LinkedInPage(getDriver());
    }

    public FacebookPage openFacebookPageInNewWindow() {
        facebookButton.click();
        waitElement();
        switchToNewTab();
        return new FacebookPage(getDriver());
    }

    public TwitterPage openTwitterPageInNewWindow() {
        twitterButton.click();
        waitElement();
        switchToNewTab();
        return new TwitterPage(getDriver());
    }

    public boolean isDisplayedWhatWeDoButton(){
        return whatWeDoButton.isDisplayed();
    }

    public boolean isDisplayedWhoWeAreButton(){
        return whatWeDoButton.isDisplayed();
    }

    public boolean isDisplayedWorkWithUsButton(){
        return workWithUsButton.isDisplayed();
    }

    public boolean isDisplayedCareersButton(){
        return careersButton.isDisplayed();
    }

    public boolean isDisplayedInvestorRelationButton(){
        return investorRelationsButton.isDisplayed();
    }

    public boolean isDisplayedPressButton(){
        return pressButton.isDisplayed();
    }

    public boolean isDisplayedMagazineButton(){
        return magazineButton.isDisplayed();
    }

    public void clickWhatWeDoButton(){
        whatWeDoButton.click();
    }

    public void clickWhoWeAreButton(){
        whatWeDoButton.click();
    }

    public void clickWorkWithUsButton(){
        workWithUsButton.click();
    }

    public void clickCareersButton(){
        careersButton.click();
    }

    public void clickInvestorRelationButton(){
        investorRelationsButton.click();
    }

    public void clickPressButton(){
        pressButton.click();
    }

    public void clickMagazineButton(){
        magazineButton.click();
    }

    public boolean isDisplayedWhatWeDoPage(){
        return whatWeDoPage.isDisplayed();
    }

    public boolean isDisplayedWhoWeArePage(){
        return whatWeDoPage.isDisplayed();
    }

    public boolean isDisplayedWorkWithUsPage(){
        return workWithUsPage.isDisplayed();
    }

    public boolean isDisplayedCareersPage(){
        return careersPage.isDisplayed();
    }

    public boolean isDisplayedInvestorRelationPage(){
        return investorRelationsPage.isDisplayed();
    }

    public boolean isDisplayedPressPage(){
        return pressPage.isDisplayed();
    }

    public boolean isDisplayedMagazinePage(){
        return magazinePage.isDisplayed();
    }
}
